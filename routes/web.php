<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/tes', function(){
    $percobaan = "Halo Galih, Are you Okay?";
    return view('test');
});

Route::get('/post', 'PostController@index');
Route::get('/post/create', 'PostController@create');
Route::post('/newpost', 'PostController@handlePost');

Route::get('/master', function () {
    return view('master');
});

Route::get('/posts/create', 'PostController@create');
Route::post('/post','PostController@store');